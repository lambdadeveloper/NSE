Noções de Segurança Eletrônica
==============================

Esboço de monitoramento simples utilizando aplicativos OpenSource em ambiente Linux/Unix - como prova de conceito será utilizado a distribuição Debian.

## Requisitos
- Debian 8 ou superior (testes realizados com sucesso utilizando Docker containers com Ubuntu 8+, CentOS e Arch);
- Gerenciador de pacotes APT;
- Navegador web gráfico (Chrome, Firefox, etc);
- Editor de textos;

## Instalação

A instalação utilizando o gerenciador de pacotes pode ser realizada com o comando `sudo apt-get install motion` e após instalado, com as configurações padrões, o aplicativo já é capaz de realizar o streaming da captura da câmera principal (localizada geralmente em `/dev/video0`) executando o comando `sudo motion`.

Para acessar o streaming certifique-se de que nenhum firewall esteja barrando a porta `8081` e acesse o endereço web de seu navegador web gráfico `http://localhost:8081` ou `http://127.0.0.1/8081` (ou caso tenha configurado outro *alias* para sua máquina).

## Configuração básica de exemplo


### Definição do quadrante observado

Utilizarei como exemplo o rastreio de apenas o canto inferior direito que, quando perceber movimentação deve executar um comando específico enviando um sinal para o servidor de alertas.

Para isso será necessário alterar o arquivo `/etc/motion/motion.conf`, descomentando a linha referente ao `area_detect`, setando conforme o exemplo abaixo¹:

```
# Detect motion in predefined areas (1 - 9). Areas are numbered like that:  1 2 3
# A script (on_area_detected) is started immediately when motion is         4 5 6
# detected in one of the given areas, but only once during an event.        7 8 9
# One or more areas can be specified with this option. Take care: This option
# does NOT restrict detection to these areas! (Default: not defined)
area_detect 9
```

_¹: para mais informações verificar documentação._

### Executar comando ao detectar movimento

Ao verificar que houve movimentação no quadrante desejado será executado o comando `call_the_guards` informando como parâmetros o caminho da imagem capturada juntamente de data e hora.


#### Movimentação em área predefinida
```
# Command to be executed when motion in a predefined area is detected
# Check option 'area_detect'.   (default: none)
on_area_detected call_the_guards "área predefinida" "%Y-%m-%d %H:%M:%S"
```

#### Movimentação detectada naquele dispositivo
```
# Command to be executed when a motion frame is detected (default: none)
on_motion_detected call_the_guards "movimentação no setor" "%Y-%m-%d %H:%M:%S"
```

#### Conteúdo do script: call_the_guards

O arquivo deve ser salvo em `/bin/call_the_guards`, com permissões de execução `chmod +x /bin/call_the_guards` contendo as seguintes instruções:

```
#!/bin/bash

echo "$*" >> /home/guards
```


### Ajuste fino

Fez-se necessário reduzir o intervalo de detecção em segundos entre um evento de detecção e outro afim de possibilitar múltiplos testes.

```
# Event Gap is the seconds of no motion detection that triggers the end of an event.
# An event is defined as a series of motion images taken within a short timeframe.
# Recommended value is 60 seconds (Default). The value -1 is allowed and disables
# events causing all Motion to be written to one single movie file and no pre_capture.
# If set to 0, motion is running in gapless mode. Movies don't have gaps anymore. An
# event ends right after no more motion is detected and post_capture is over.
event_gap 3
```

## Detalhes sobre os pacotes utilizados
### Motion

```
Package: motion
Version: 4.0-1
Priority: optional
Section: universe/graphics
Origin: Ubuntu
Maintainer: Ubuntu Developers <ubuntu-devel-discuss@lists.ubuntu.com>
Original-Maintainer: Ximin Luo <infinity0@debian.org>
Bugs: https://bugs.launchpad.net/ubuntu/+filebug
Installed-Size: 729 kB
Depends: libavcodec57 (>= 7:3.1.5) | libavcodec-extra57 (>= 7:3.1.5), libavformat57 (>= 7:3.1.5), libavutil55 (>= 7:3.1.5), libc6 (>= 2.15), libjpeg8 (>= 8c), libmysqlclient20 (>= 5.7.11), libpq5, libsqlite3-0 (>= 3.6.0), libswscale4 (>= 7:3.1.5), debconf (>= 0.5) | debconf-2.0, lsb-base (>= 3.0-6), adduser
Recommends: ffmpeg
Suggests: default-mysql-client, postgresql-client
Homepage: https://github.com/Motion-Project/motion
Download-Size: 236 kB
APT-Manual-Installed: yes
APT-Sources: http://archive.ubuntu.com/ubuntu zesty/universe amd64 Packages
Description: V4L capture program supporting motion detection
 Motion is a program that monitors the video signal from
 one or more cameras and is able to detect if a significant
 part of the picture has changed. Or in other words, it can
 detect motion.
 .
 Motion is a command line based tool. It has no graphical
 user interface. Everything is setup either via the
 command line or via configuration files.
 .
 The output from motion can be:
    - jpg/ppm image files
    - mpeg/mp4/swf/flv/mov/ogg video sequences
 .
 Also, motion has its own minimalistic web server. Thus,
 you can access the webcam output from motion via a browser.

```
